﻿using BusinessLogic;
using Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MapReduce.Controllers
{
    public class NonMapReduceController : Controller
    {
        private readonly Dummy dummy;
        private readonly WithoutMapAndReduce withoutMapAndReduce;
        private readonly WithoutMapAndReduce2 withoutMapAndReduce2;
        private readonly WithoutMapAndReduce3 withoutMapAndReduce3;

        private static IEnumerable<SchoolClass> schoolClasses;

        public NonMapReduceController()
        {
            this.dummy = new Dummy();
            this.withoutMapAndReduce = new WithoutMapAndReduce();
            this.withoutMapAndReduce2 = new WithoutMapAndReduce2();
            this.withoutMapAndReduce3 = new WithoutMapAndReduce3();
        }

        public ActionResult Index()
        {
            int amountOfDummyValues = 500;

            schoolClasses = dummy.GenerateDummyValues(amountOfDummyValues);

            return View(schoolClasses);
        }

        [HttpPost]
        public JsonResult FemaleStudentsCount()
        {
            Stopwatch watch = Stopwatch.StartNew();

            //locally way
            int result = withoutMapAndReduce.TriggerMapAndReduce();

            //get elapsed time
            watch.Stop();
            object[] finalResult = new object[] { result, watch.Elapsed.TotalSeconds };
            return Json(finalResult);
        }

        [HttpPost]
        public JsonResult SearchAfterLastname(string searchName)
        {
            Stopwatch watch = Stopwatch.StartNew();

            if (searchName.Trim().Length > 0)
            {
                //locally way
                int result = withoutMapAndReduce2.TriggerMapAndReduce(searchName.Trim());

                //get elapsed time
                watch.Stop();
                object[] finalResult = new object[] { result, watch.Elapsed.TotalSeconds };
                return Json(finalResult);
            }

            return new JsonResult();
        }

        [HttpPost]
        public JsonResult MinorStudents()
        {
            Stopwatch watch = Stopwatch.StartNew();

            //locally way
            int result = withoutMapAndReduce3.TriggerMapAndReduce();

            //get elapsed time
            watch.Stop();
            object[] finalResult = new object[] { result, watch.Elapsed.TotalSeconds };
            return Json(finalResult);
        }
    }
}