﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using Models;

namespace MapReduce.Controllers
{
    public class HomeController : Controller
    {
        private readonly Dummy dummy;
        private readonly ICollection<ServerSocket> serverSockets;
        private readonly ReduceValuesFromClients reduceValuesFromClients;
        private readonly HandleIpAddresses handleIpAddresses;

        public HomeController()
        {
            this.dummy = new Dummy();

            this.reduceValuesFromClients = new ReduceValuesFromClients();
            this.handleIpAddresses = new HandleIpAddresses();
            this.serverSockets = new List<ServerSocket>();

            HookUpClients();
        }

        private void HookUpClients()
        {
            //fill collection with all client IPv4 addresses
            string[] clientIpAddresses = handleIpAddresses.GetSavedIpAddressesFromTxtFile();
            foreach (string clientIpAddress in clientIpAddresses)
            {
                this.serverSockets.Add(new ServerSocket(clientIpAddress));
            }
        }

        public ActionResult Index()
        {
            int amountOfDummyValues = 500;

            IEnumerable<SchoolClass> schoolClasses = dummy.GenerateDummyValues(amountOfDummyValues);

            return View(schoolClasses);
        }

        [HttpPost]
        public JsonResult FemaleStudentsCount()
        {
            Stopwatch watch = Stopwatch.StartNew();

            string searchRequest = "1|";

            IEnumerable<int> resultFromAllClients = GetAllPartialResultsFromClients(searchRequest);

            //reduce
            int result = reduceValuesFromClients.Reduce(resultFromAllClients);

            //get elapsed time
            watch.Stop();
            object[] finalResult = new object[] { result, watch.Elapsed.TotalSeconds };
            return Json(finalResult);
        }

        [HttpPost]
        public JsonResult SearchAfterLastname(string searchName)
        {
            if (searchName.Trim().Length > 0)
            {
                Stopwatch watch = Stopwatch.StartNew();

                string searchRequest = "2|" + searchName.Trim();

                IEnumerable<int> resultFromAllClients = GetAllPartialResultsFromClients(searchRequest);

                //reduce
                int result = reduceValuesFromClients.Reduce(resultFromAllClients);

                //get elapsed time
                watch.Stop();
                object[] finalResult = new object[] { result, watch.Elapsed.TotalSeconds };
                return Json(finalResult);
            }

            return new JsonResult();
        }

        [HttpPost]
        public JsonResult MinorStudents()
        {
            Stopwatch watch = Stopwatch.StartNew();

            string searchRequest = "3|";

            IEnumerable<int> resultFromAllClients = GetAllPartialResultsFromClients(searchRequest);

            //reduce
            int result = reduceValuesFromClients.Reduce(resultFromAllClients);

            //get elapsed time
            watch.Stop();
            object[] finalResult = new object[] { result, watch.Elapsed.TotalSeconds };
            return Json(finalResult);
        }

        //returns a sequence of all partial results from all clients
        private IEnumerable<int> GetAllPartialResultsFromClients(string searchRequest)
        {
            ICollection<Task<IEnumerable<int>>> taskCollection = new List<Task<IEnumerable<int>>>();

            //call all clients
            foreach (ServerSocket serverSocket in serverSockets)
            {
                taskCollection.Add(Task<IEnumerable<int>>.Factory.StartNew(() => serverSocket.TriggerClientSearch(searchRequest)));
            }

            //wait for all clients to answer
            Task.WhenAll(taskCollection);

            //merge and return all client results
            return taskCollection.SelectMany(x => x.Result);
        }
    }
}