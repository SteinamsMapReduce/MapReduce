﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;

namespace MapReduce.Controllers
{
    public class ConfigController : Controller
    {
        private readonly HandleIpAddresses handleIpAddresses;

        public ConfigController()
        {
            this.handleIpAddresses = new HandleIpAddresses();
        }

        public ActionResult Index()
        {
            IEnumerable<string> ipAddresses = handleIpAddresses.GetSavedIpAddressesFromTxtFile().OrderBy(x => x);
            return View(ipAddresses);
        }

        [HttpPost]
        public JsonResult SaveIpAddresses(string ipAddresses)
        {
            if (ModelState.IsValid)
            {
                bool result = handleIpAddresses.StoreIpAddresses(ipAddresses);

                IEnumerable<string> newIpAddressesSequence = handleIpAddresses.GetSavedIpAddressesFromTxtFile().OrderBy(x => x);

                object[] finalResult = new object[] { result, newIpAddressesSequence };
                return Json(finalResult);
            }
            return Json(false);
        }
	}
}