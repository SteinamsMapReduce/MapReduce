﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MapReduce.Startup))]
namespace MapReduce
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
