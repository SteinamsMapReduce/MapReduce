﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapReduceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "MapReduce Beispielanwendung Client";

            //generate dummy values
            Dummy dummy = new Dummy();
            IEnumerable<SchoolClass> schoolClasses = dummy.GenerateDummyValues(1000000);    //1 million dummy objects

            //starting client tcp listener            
            ClientSocket clientSocket = new ClientSocket();
            clientSocket.ClientTcpListener(schoolClasses);
        }
    }
}
