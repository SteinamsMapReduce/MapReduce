﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapReduceClient
{
    public class Mapping
    {
        public IEnumerable<int> MapCountAllFemaleStudents(IEnumerable<SchoolClass> schoolClasses)
        {
            ConcurrentBag<int> mappedCollection = new ConcurrentBag<int>();

            Parallel.ForEach(schoolClasses, schoolClass =>
            {
                //count all female students in all classes
                int amountOfFemaleStudents = schoolClass.Students.Count(x => x.Gender == Gender.Female);

                mappedCollection.Add(amountOfFemaleStudents);
            });

            return mappedCollection;
        }

        public IEnumerable<int> MapSearchForLastname(IEnumerable<SchoolClass> schoolClasses, string searchName)
        {
            ConcurrentBag<int> mappedCollection = new ConcurrentBag<int>();

            Parallel.ForEach(schoolClasses, schoolClass =>
            {
                //get amount of all students which match LastName to searchName
                int amountOfFemaleStudents = schoolClass.Students.Count(x => x.LastName == searchName);

                //store only non-empty results
                if (amountOfFemaleStudents > 0)
                {
                    mappedCollection.Add(amountOfFemaleStudents);
                }
            });

            return mappedCollection;
        }

        public IEnumerable<int> MapCountAllMinorStudents(IEnumerable<SchoolClass> schoolClasses)
        {
            ConcurrentBag<int> mappedCollection = new ConcurrentBag<int>();

            Parallel.ForEach(schoolClasses, schoolClass =>
            {
                //count all minor students (students below age of 18) in all classes
                int amountOfFemaleStudents = schoolClass.Students.Count(x => x.Age < 18);

                mappedCollection.Add(amountOfFemaleStudents);
            });

            return mappedCollection;
        }
    }
}