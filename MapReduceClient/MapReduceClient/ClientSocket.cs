﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace MapReduceClient
{
    public class ClientSocket
    {
        private Mapping mapping;
        private TcpClient clientSocket;
        private TcpListener serverSocket;
        private Encoding latin1encoding;

        private IEnumerable<SchoolClass> schoolClasses;

        public ClientSocket()
        {
            this.mapping = new Mapping();
            this.clientSocket = new TcpClient();
            this.latin1encoding = Encoding.GetEncoding("iso-8859-1");

            EstablishServerSocket();
        }

        private void EstablishServerSocket()
        {
            //set client IPv4 address
            IPAddress ipAddress = GetLocalIpv4Address();
            const int port = 50505;

            IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, port);

            this.serverSocket = new TcpListener(ipEndPoint);
        }

        private IPAddress GetLocalIpv4Address()
        {
            try
            {
                return Dns.GetHostEntry(Dns.GetHostName())
                    .AddressList
                    .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
            }
            catch
            {
                string ip = "127.0.0.1";
                IPAddress ipAddress;
                IPAddress.TryParse(ip, out ipAddress);
                return ipAddress;
            }
        }

        //listens for tcp queries from server
        public void ClientTcpListener(IEnumerable<SchoolClass> schoolClasses)
        {
            Console.WriteLine("Ready to rumble...");

            while (true)
            {
                serverSocket.Start();

                //wait for call
                using (clientSocket = serverSocket.AcceptTcpClient())
                {
                    Console.Write("Incoming server request... ");
                    try
                    {
                        this.schoolClasses = schoolClasses;

                        //get network stream
                        NetworkStream networkStream = clientSocket.GetStream();

                        //get search request
                        string request = GetSearchNameFromServer(networkStream);

                        byte[] resultStream = GetSearchResult(request);

                        //send search result to server
                        networkStream.Write(resultStream, 0, resultStream.Length);
                        networkStream.Flush();

                        Console.WriteLine(" ...served");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());

                        //wait 15 seconds
                        Thread.Sleep(15000);

                        //restart method using recursion
                        ClientTcpListener(schoolClasses);
                    }
                }
            }
        }

        //get name for search from server
        private string GetSearchNameFromServer(NetworkStream networkStream)
        {
            //read server request
            byte[] inputStream = new byte[65536];
            networkStream.Read(inputStream, 0, clientSocket.ReceiveBufferSize);

            //get rid of empty bytes
            byte[] decodedBytes = Decode(inputStream);

            //return requested search name
            return this.latin1encoding.GetString(decodedBytes);
        }

        //gets and returns requested search result as a byte array
        private byte[] GetSearchResult(string requested)
        {
            string[] searchRequest = requested.Split('|');

            if (searchRequest.Length == 2)
            {
                //select kind of request
                switch (searchRequest[0])
                {
                    case "1":
                        {
                            return GetAllFemaleStudentsCount();
                        }
                    case "2":
                        {
                            return GetLastnameCount(searchRequest[1]);
                        }
                    case "3":
                        {
                            return GetAllMinorStudentsCount();
                        }
                    default:
                        {
                            throw new Exception("unclear parameters");
                        }
                }
            }

            throw new Exception("unclear parameters");
        }

        //get search results for all female students
        private byte[] GetAllFemaleStudentsCount()
        {
            //get search result from map method
            IEnumerable<int> mappedSequence = this.mapping.MapCountAllFemaleStudents(schoolClasses);

            //return byte array of search result
            return GetByteArray(mappedSequence);
        }

        //get search results for requested name
        private byte[] GetLastnameCount(string searchName)
        {
            //get search result from map method
            IEnumerable<int> mappedSequence = this.mapping.MapSearchForLastname(schoolClasses, searchName);

            //return byte array of search result
            return GetByteArray(mappedSequence);
        }

        //get search results for all female students
        private byte[] GetAllMinorStudentsCount()
        {
            //get search result from map method
            IEnumerable<int> mappedSequence = this.mapping.MapCountAllMinorStudents(schoolClasses);

            //return byte array of search result
            return GetByteArray(mappedSequence);
        }

        //returns an array of bytes for input sequence
        private byte[] GetByteArray(IEnumerable<int> mappedSequence)
        {
            //get comma separated string with search result
            string mappedSearchResults = String.Join(",", mappedSequence);

            //return byte array of search result
            return Encoding.ASCII.GetBytes(mappedSearchResults);
        }

        //removes the empty bytes at the end of input byte array and returns trimmed array
        private byte[] Decode(byte[] input)
        {
            int i = input.Length - 1;
            while (input[i] == 0 && i > 0)
            {
                --i;
            }
            byte[] temp = new byte[i + 1];

            Array.Copy(input, temp, i + 1);

            return temp;
        }
    }
}