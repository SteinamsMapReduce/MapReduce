﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MapReduceClient
{
    public struct Student
    {
        //public int StudentId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public int Age { get; set; }
    }
}