﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MapReduceClient
{
    public class Dummy
    {
        private ThreadLocal<Random> localRandom = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));

        private readonly char[] lowerCaseCharacters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

        public IEnumerable<SchoolClass> GenerateDummyValues(int amountOfDummyValues)
        {
            ConcurrentBag<SchoolClass> schoolClasses = new ConcurrentBag<SchoolClass>();

            Parallel.For(0, amountOfDummyValues, i =>
            {
                schoolClasses.Add(new SchoolClass
                {
                    BuildingNumber = localRandom.Value.Next(-1, 4),
                    Floor = localRandom.Value.Next(0, 10),
                    NameOfClass = GenerateName(5),
                    RoomName = GenerateName(10),
                    Students = GenerateStudents()
                });
            });

            return schoolClasses;
        }

        //returns a random name for both NameOfClass and RoomName
        private string GenerateName(int amountOfCharacters)
        {
            StringBuilder stringBuilder = new StringBuilder();

            char character;
            for (int i = 0; i < amountOfCharacters; i++)
            {
                character = lowerCaseCharacters[localRandom.Value.Next(0, 26)];
                stringBuilder.Append(character);
            }

            return stringBuilder.ToString();
        }

        //returns a collection of random generated Student objects
        private ICollection<Student> GenerateStudents()
        {
            int amountOfStudents = localRandom.Value.Next(15, 35);

            ICollection<Student> students = new List<Student>();

            for (int i = 0; i < amountOfStudents; i++)
            {
                Gender gender = (Gender)localRandom.Value.Next(0, 2);

                students.Add(new Student
                {
                    Age = localRandom.Value.Next(16, 46),
                    Gender = gender,
                    FirstName = (gender == Gender.Male)
                        ? Enum.GetName(typeof(MaleFirstName), localRandom.Value.Next(0, 501))
                        : Enum.GetName(typeof(FemaleFirstName), localRandom.Value.Next(0, 501)),
                    LastName = Enum.GetName(typeof(LastName), localRandom.Value.Next(0, 101))
                });
            }

            return students;
        }
    }
}