﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MapReduceClient
{
    class HttpListenerTest
    {
        private Dummy dummy;
        private Mapping mapping;

        public HttpListenerTest()
        {
            this.dummy = new Dummy();
            this.mapping = new Mapping();
        }

        public void Test()
        {
            IEnumerable<SchoolClass> schoolClasses = this.dummy.GenerateDummyValues(1000);


            //string searchName = Encoding.ASCII.GetString(inStream);

            //get search result
            //IEnumerable<int> mappedCollection = this.mapping.Map(schoolClasses, searchName);

            //send search result
            //string test = String.Join(",", mappedCollection);

            //byte[] outStream = Encoding.ASCII.GetBytes(test);

            ListenAsync(); // Start server
            WebClient wc = new WebClient(); // Make a client request.
            //wc.UploadData(ip, outStream);
            //Console.WriteLine(wc.DownloadString("http://localhost:51111/Request.txt"));


        }

        async static void ListenAsync()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:51111/"); // Listen on
            listener.Start(); // port 51111.
                              // Await a client request:

            HttpListenerContext context = await listener.GetContextAsync();
            // Respond to the request:
            string msg = "You asked for: " + context.Request.RawUrl;
            context.Response.ContentLength64 = Encoding.UTF8.GetByteCount(msg);
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            using (Stream s = context.Response.OutputStream)
            {
                using (StreamWriter writer = new StreamWriter(s))
                {
                    await writer.WriteAsync(msg);
                }
            }
            listener.Stop();
        }
    }
}