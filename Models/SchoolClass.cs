﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public struct SchoolClass
    {
        //public int SchoolClassId { get; set; }

        public string NameOfClass { get; set; }

        public string RoomName { get; set; }

        public int BuildingNumber { get; set; }

        public int Floor { get; set; }

        public ICollection<Student> Students { get; set; }
    }
}