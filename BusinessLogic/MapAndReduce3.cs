﻿using Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BusinessLogic
{
    [Obsolete]
    public class MapAndReduce3
    {
        private ConcurrentBag<int> mappedCollection = new ConcurrentBag<int>();

        public int TriggerMapAndReduce(IEnumerable<SchoolClass> schoolClasses)
        {
            //first step
            Map(schoolClasses);

            //second step
            int result = Reduce();

            //return calculated result
            return result;
        }

        private void Map(IEnumerable<SchoolClass> schoolClasses)
        {
            Parallel.ForEach(schoolClasses, schoolClass =>
            {
                int amountOfFemaleStudents = schoolClass.Students.Count(x => x.Age < 18);
                this.mappedCollection.Add(amountOfFemaleStudents);
            });
        }

        private int Reduce()
        {
            return mappedCollection.Sum();
        }
    }
}