﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLogic
{
    public class ReduceValuesFromClients
    {
        public int Reduce(params IEnumerable<int>[] sequences)
        {
            int result = 0;

            foreach (IEnumerable<int> sequence in sequences)
            {
                result = sequence.Sum();
            }

            return result;
        }
    }
}