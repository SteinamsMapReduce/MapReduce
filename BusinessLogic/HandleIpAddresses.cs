﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;

namespace BusinessLogic
{
    public class HandleIpAddresses
    {
        private readonly string directory;
        private readonly string path;

        public HandleIpAddresses()
        {
            this.directory = System.AppDomain.CurrentDomain.BaseDirectory;
            this.path = directory + @"\clientIpAddresses.txt";
        }

        public string[] GetSavedIpAddressesFromTxtFile()
        {
            try
            {
                return File.ReadAllLines(path);
            }
            catch
            {
                return new string[0];
            }
        }
        
        public bool StoreIpAddresses(string ipAddresses)
        {
            string trimmedIpAddresses = ipAddresses.Trim();
            IEnumerable<string> sequenceOfAllIpAddresses = trimmedIpAddresses.Split('\n');

            IEnumerable<string> checkedIpAddresses = CheckIpv4Addresses(sequenceOfAllIpAddresses);

            return SaveIpAddressesToTxtFile(checkedIpAddresses);
        }

        private bool SaveIpAddressesToTxtFile(IEnumerable<string> ipAddresses)
        {
            try
            {
                File.WriteAllLines(path, ipAddresses);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private IEnumerable<string> CheckIpv4Addresses(IEnumerable<string> sequenceOfAllIpAddresses)
        {
            char[] charactersToTrim = ",;\".".ToCharArray();

            return sequenceOfAllIpAddresses
                .Select(x => x.Trim(charactersToTrim))
                .Where(x => IsIpv4AddressValid(x));
        }

        private bool IsIpv4AddressValid(string ipAddress)
        {
            IPAddress address;
            return IPAddress.TryParse(ipAddress, out address);
        }

        public int GetAmountOfClients()
        {
            string[] allClientIpAddresses = GetSavedIpAddressesFromTxtFile();
            return allClientIpAddresses.Length;
        }
    }
}