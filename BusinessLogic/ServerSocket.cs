﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace BusinessLogic
{
    public class ServerSocket
    {
        private TcpClient clientSocket;
        private Encoding latin1encoding;

        private readonly string ip;
        private const int port = 50505;

        public ServerSocket(string ip)
        {
            this.clientSocket = new TcpClient();
            this.latin1encoding = Encoding.GetEncoding("iso-8859-1");

            //set ip address of client
            this.ip = ip;
        }

        //trigger search for request on client
        public IEnumerable<int> TriggerClientSearch(string searchRequest)
        {
            try
            {
                //establish connection to client
                this.clientSocket.Connect(ip, port);

                using (NetworkStream serverStream = this.clientSocket.GetStream())
                {
                    //send request to client
                    byte[] sendBytes = latin1encoding.GetBytes(searchRequest);
                    serverStream.Write(sendBytes, 0, searchRequest.Length);

                    //get result from client
                    int maxListeningValue = 3000000;
                    byte[] resultStream = new byte[maxListeningValue];
                    serverStream.Read(resultStream, 0, maxListeningValue);

                    //get rid of empty bytes at the end of result
                    byte[] decodedBytes = Decode(resultStream);

                    string dataFromClient = Encoding.ASCII.GetString(decodedBytes);

                    //get sequence of client result
                    int integerParsingCheck;
                    return dataFromClient
                        .Split(',')
                        .Where(x => Int32.TryParse(x, out integerParsingCheck))
                        .Select(Int32.Parse);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return new List<int>();
            }
        }

        //removes the empty bytes at the end of input byte array and returns array
        public byte[] Decode(byte[] input)
        {
            int i = input.Length - 1;
            while (input[i] == 0 && i > 0)
            {
                --i;
            }
            byte[] temp = new byte[i + 1];

            Array.Copy(input, temp, i + 1);

            return temp;
        }
    }
}