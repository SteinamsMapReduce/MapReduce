﻿using Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BusinessLogic
{
    public class WithoutMapAndReduce
    {
        private readonly Dummy dummy;
        private readonly HandleIpAddresses handleIpAddresses;

        public WithoutMapAndReduce()
        {
            this.dummy = new Dummy();
            this.handleIpAddresses = new HandleIpAddresses();
        }

        public int TriggerMapAndReduce()
        {
            try
            {
                int amountOfDummyValues = GetAmountOfValues();

                IEnumerable<SchoolClass> schoolClasses = dummy.GenerateDummyValues(amountOfDummyValues);
                
                List<int> list = new List<int>();
                
                foreach (SchoolClass schoolClass in schoolClasses)
                {
                    int amountOfFemaleStudents = schoolClass.Students.Where(x => x.Gender == Gender.Female).Count();

                    list.Add(amountOfFemaleStudents);
                }

                return list.Sum();
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine(ex);
                return -2;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
        }

        //returns the amount of dummy values
        private int GetAmountOfValues()
        {
            int amountOfClients = handleIpAddresses.GetAmountOfClients();
            int amountOfDummyValuesPerClient = 1000000;
            int amountOfDummyValues = 1000000;

            checked
            {
                amountOfDummyValues = amountOfClients * amountOfDummyValuesPerClient;
            }

            return amountOfDummyValues;
        }
    }
}