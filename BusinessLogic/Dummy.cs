﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Models;

namespace BusinessLogic
{
    public class Dummy
    {
        private ThreadLocal<Random> localRandom = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));
        
        private readonly char[] lowerCaseCharacters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

        public IEnumerable<SchoolClass> GenerateDummyValues(int amountOfDummyValues)
        {
            ConcurrentBag<SchoolClass> schoolClasses = new ConcurrentBag<SchoolClass>();

            Parallel.For(0, amountOfDummyValues, i =>
            {
                schoolClasses.Add(new SchoolClass
                {
                    BuildingNumber = localRandom.Value.Next(-1, 4),
                    Floor = localRandom.Value.Next(0, 10),
                    NameOfClass = GenerateNameOfClass(),
                    RoomName = GenerateRoomName(),
                    Students = GenerateStudents()
                });
            });

            return schoolClasses;
        }

        //returns a random name for NameOfClass
        private string GenerateNameOfClass()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(localRandom.Value.Next(10, 14));

            for (int i = 0; i < 2; i++)
            {
                char character = lowerCaseCharacters[localRandom.Value.Next(0, 26)];
                stringBuilder.Append(char.ToUpper(character));
            }

            stringBuilder.Append(localRandom.Value.Next(1, 5));

            return stringBuilder.ToString();
        }

        //returns a random name RoomName
        private string GenerateRoomName()
        {
            StringBuilder stringBuilder = new StringBuilder();

            char character = lowerCaseCharacters[localRandom.Value.Next(0, 26)];
            stringBuilder.Append(char.ToUpper(character));

            stringBuilder.Append(0);
            stringBuilder.Append(localRandom.Value.Next(1, 99).ToString("D2"));

            return stringBuilder.ToString();
        }

        //returns a collection of random generated Student objects
        private ICollection<Student> GenerateStudents()
        {
            int amountOfStudents = localRandom.Value.Next(15, 35);

            ICollection<Student> students = new Collection<Student>();

            for (int i = 0; i < amountOfStudents; i++)
            {
                Gender gender = (Gender)localRandom.Value.Next(0, 2);

                students.Add(new Student
                {
                    Age = localRandom.Value.Next(16, 46),
                    Gender = gender,
                    FirstName = (gender == Gender.Male)
                        ? Enum.GetName(typeof(MaleFirstName), localRandom.Value.Next(0, 501))
                        : Enum.GetName(typeof(FemaleFirstName), localRandom.Value.Next(0, 501)),
                    LastName = Enum.GetName(typeof(LastName), localRandom.Value.Next(0, 101))
                });
            }

            return students;
        }
    }
}