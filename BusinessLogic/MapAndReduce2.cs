﻿using Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BusinessLogic
{
    [Obsolete]
    public class MapAndReduce2
    {
        private ConcurrentBag<int> mappedCollection = new ConcurrentBag<int>();

        public int TriggerMapAndReduce(IEnumerable<SchoolClass> schoolClasses, string searchName)
        {
            //first step
            Map(schoolClasses, searchName);

            //second step
            int result = Reduce();

            //return calculated result
            return result;
        }

        private void Map(IEnumerable<SchoolClass> schoolClasses, string searchName)
        {
            Parallel.ForEach(schoolClasses, schoolClass =>
                {
                    //get amount of all students which match LastName to searchName
                    int amountOfFemaleStudents = schoolClass.Students.Count(x => x.LastName == searchName);
                    this.mappedCollection.Add(amountOfFemaleStudents);
                });
        }

        private int Reduce()
        {
            return mappedCollection.Sum();
        }
    }
}